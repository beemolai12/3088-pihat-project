# 3088 PiHat Project

Git repository for EEE3088F group 6's PiHat Project 

The microHat acts as an interface between a Raspberry Pi Zero and a servo motor. This makes it possible to drive a motor with the Raspberry Pi which would otherwise not be ideal as the Raspberry Pi cannot source the required voltage to operate most motors.

Bill of materials(to be completed as design gets polished up):
So far, the components required have been added. Material for PCB to follow when PCB is designed.
https://uctcloud-my.sharepoint.com/:x:/g/personal/mlxbat001_myuct_ac_za/EWdN2vaBJ3ZEvrao2ezRMqQBerPnBH6QXVHZ_QEQuyTXOg?e=PChcXl

Circuits simulated will be uploaded.

